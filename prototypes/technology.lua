data:extend({
	{
		type = "technology",
		name = "logicarts-tech1",
		icon = "__logicarts__/graphics/cart-tech.png",
		icon_size = 128,
		effects = {
			{ type = "unlock-recipe", recipe = "logicarts-paint" },
			{ type = "unlock-recipe", recipe = "logicarts-car" },
            { type = "unlock-recipe", recipe = "logicarts-car-medium" },
            { type = "unlock-recipe", recipe = "logicarts-car-large" },
            { type = "unlock-recipe", recipe = "logicarts-car-huge" },

            { type = "unlock-recipe", recipe = "logicarts-path" },
			{ type = "unlock-recipe", recipe = "logicarts-stop" },
			{ type = "unlock-recipe", recipe = "logicarts-turn" },
			{ type = "unlock-recipe", recipe = "logicarts-turn-fuel" },
			{ type = "unlock-recipe", recipe = "logicarts-turn-blocked" },
			{ type = "unlock-recipe", recipe = "logicarts-continue" },
			{ type = "unlock-recipe", recipe = "logicarts-yield" }
		},
		prerequisites = {
			"engine",
			"logistics",
		},
		unit = {
			count = 100,
			ingredients = {
				{"science-pack-1", 1},
				{"science-pack-2", 1}
			},
			time = 30
		},
		order = "c-o-a",
	},
	{
		type = "technology",
		name = "logicarts-tech2",
		icon = "__logicarts__/graphics/e-cart-tech.png",
		icon_size = 128,
		effects = {
			{ type = "unlock-recipe", recipe = "logicarts-car-electric" },
            { type = "unlock-recipe", recipe = "logicarts-car-electric-medium" },
            { type = "unlock-recipe", recipe = "logicarts-car-electric-large" },
            { type = "unlock-recipe", recipe = "logicarts-car-electric-huge" }
		},
		prerequisites = {
			"electric-engine",
			"advanced-electronics",
			"logicarts-tech1",
		},
		unit = {
			count = 200,
			ingredients = {
				{"science-pack-1", 1},
				{"science-pack-2", 1},
				{"science-pack-3", 1},
			},
			time = 30
		},
		order = "c-o-a",
	},
	{
		type = "technology",
		name = "logicarts-tech-stops",
		icon = "__logicarts__/tech-stops.png",
		icon_size = 128,
		effects = {
			{ type = "unlock-recipe", recipe = "logicarts-stop-load" },
			{ type = "unlock-recipe", recipe = "logicarts-stop-unload" },
			{ type = "unlock-recipe", recipe = "logicarts-stop-supply" },
			{ type = "unlock-recipe", recipe = "logicarts-stop-dump" },
			{ type = "unlock-recipe", recipe = "logicarts-stop-accept" },
		},
		prerequisites = {
			"logicarts-tech1",
		},
		unit = {
			count = 150,
			ingredients = {
				{"science-pack-1", 1},
				{"science-pack-2", 1}
			},
			time = 30
		},
		order = "c-o-a",
	},
	{
		type = "technology",
		name = "logicarts-tech-stickers",
		icon = "__logicarts__/tech-stickers.png",
		icon_size = 128,
		effects = {
			{ type = "unlock-recipe", recipe = "logicarts-sticker" },
		},
		prerequisites = {
			"logicarts-tech1",
		},
		unit = {
			count = 150,
			ingredients = {
				{"science-pack-1", 1},
				{"science-pack-2", 1}
			},
			time = 30
		},
		order = "c-o-a",
	},
	{
		type = "technology",
		name = "logicarts-tech-groups",
		icon = "__logicarts__/tech-groups.png",
		icon_size = 128,
		effects = {
			{ type = "unlock-recipe", recipe = "logicarts-equipment-1" },
			{ type = "unlock-recipe", recipe = "logicarts-equipment-2" },
			{ type = "unlock-recipe", recipe = "logicarts-equipment-3" },
			{ type = "unlock-recipe", recipe = "logicarts-equipment-4" },
			{ type = "unlock-recipe", recipe = "logicarts-equipment-5" },
		},
		prerequisites = {
			"logicarts-tech1",
		},
		unit = {
			count = 150,
			ingredients = {
				{"science-pack-1", 1},
				{"science-pack-2", 1}
			},
			time = 30
		},
		order = "c-o-a",
	},
	{
		type = "technology",
		name = "logicarts-tech-dual",
		icon = "__logicarts__/tech-dual.png",
		icon_size = 128,
		effects = {
			{ type = "unlock-recipe", recipe = "logicarts-path-dual-straight" },
			{ type = "unlock-recipe", recipe = "logicarts-path-dual-turn" },
			{ type = "unlock-recipe", recipe = "logicarts-continue-dual-straight" },
			{ type = "unlock-recipe", recipe = "logicarts-continue-dual-turn" },
		},
		prerequisites = {
			"logicarts-tech1",
		},
		unit = {
			count = 150,
			ingredients = {
				{"science-pack-1", 1},
				{"science-pack-2", 1}
			},
			time = 30
		},
		order = "c-o-a",
	},
	{
		type = "technology",
		name = "logicarts-tech-chests",
		icon = "__logicarts__/graphics/cart-chest-tech.png",
		effects = {
			{
				type = "unlock-recipe",
				recipe = "logicarts-car-chest-provider"
			},
			{
				type = "unlock-recipe",
				recipe = "logicarts-car-chest-requester"
			},
		},
		icon_size = 128,
		prerequisites =
		{
			"logistic-system",
			"logicarts-tech1"
		},
		unit = {
			count = 150,
			ingredients = {
				{"science-pack-1", 1},
				{"science-pack-2", 1},
				{"science-pack-3", 1},
			},
			time = 30
		},
		order = "c-k-d",
	},
	{
		type = "technology",
		name = "logicarts-tech-bridge",
		icon = "__logicarts__/graphics/cart-bridge-tech.png",
		effects = {
			{
				type = "unlock-recipe",
				recipe = "logicarts-cart-tunnel"
			},
			{
				type = "unlock-recipe",
				recipe = "logicarts-path-fast-ns"
			},
			{
				type = "unlock-recipe",
				recipe = "logicarts-path-fast-ew"
			},
			{
				type = "unlock-recipe",
				recipe = "logicarts-path-express-ns"
			},
			{
				type = "unlock-recipe",
				recipe = "logicarts-path-express-ew"
			},
			{
				type = "unlock-recipe",
				recipe = "logicarts-path-turbo-ns"
			},
			{
				type = "unlock-recipe",
				recipe = "logicarts-path-turbo-ew"
			},
			{
				type = "unlock-recipe",
				recipe = "logicarts-path-ultimate-ns"
			},
			{
				type = "unlock-recipe",
				recipe = "logicarts-path-ultimate-ew"
			},
		},
		icon_size = 128,
		prerequisites =
		{
			"logistic-system",
			"logicarts-tech1"
		},
		unit = {
			count = 150,
			ingredients = {
				{"science-pack-1", 1},
				{"science-pack-2", 1},
				{"science-pack-3", 1},
			},
			time = 30
		},
		order = "c-k-d",
	},

})
